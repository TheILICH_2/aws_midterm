FROM golang:1.22.1-bookworm

WORKDIR /app

COPY . /app/

RUN go get .

ENTRYPOINT ["go", "run", "."]  



